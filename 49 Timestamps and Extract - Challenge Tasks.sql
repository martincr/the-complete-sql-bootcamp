
- During which months did payments occur?

SELECT * FROM payment;

SELECT * FROM payment
ORDER BY payment_date DESC;

SELECT payment_date FROM payment
ORDER BY payment_date DESC;

SELECT (EXTRACT(MONTH FROM payment_date)) FROM payment
ORDER BY payment_date DESC;

SELECT (TO_CHAR(payment_date,'Month')) FROM payment
ORDER BY payment_date DESC;

SELECT DISTINCT(TO_CHAR(payment_date,'Month')) FROM payment;

- Format your answer to return back the full month name.

SELECT * FROM payment;

SELECT COUNT(*) FROM payment;

SELECT to_char(payment_date, 'Day') FROM payment;

# Use the EXTRACT and dow:

SELECT COUNT(*) FROM payment
WHERE
EXTRACT(dow FROM payment_date) in (1);

SELECT COUNT(*) FROM payment
WHERE
EXTRACT(dow FROM payment_date) = 1;