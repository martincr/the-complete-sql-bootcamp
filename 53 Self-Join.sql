# Note: Can be thought of as a join of two copies of the same table.
# Note: Use an alias for the table.

- Find all the pairs of films that have the same length.

SELECT * FROM film;

SELECT title,length FROM film;

SELECT title,length FROM film
WHERE length = 117;

SELECT f1.title,f2.title,f1.length
FROM film AS f1
INNER JOIN film AS f2 ON
f1.film_id = f2.film_id
AND f1.length = f2.length;

SELECT f1.title,f2.title,f1.length
FROM film AS f1
INNER JOIN film AS f2 ON
f1.film_id != f2.film_id
AND f1.length = f2.length;