SELECT * FROM payment;
SELECT * FROM payment
WHERE amount > 5.00;
SELECT COUNT(amount) FROM payment
WHERE amount > 5.00;
SELECT * FROM actor;
SELECT COUNT(*) FROM actor
WHERE first_name LIKE 'P%';
SELECT * FROM address;
SELECT COUNT (DISTINCT (district)) FROM address;
SELECT DISTINCT (district) FROM address;
SELECT * FROM film;
SELECT * FROM film
WHERE rating = 'R';
SELECT COUNT(*) FROM film
WHERE rating = 'R'
AND replacement_cost BETWEEN 5 AND 15;
SELECT * FROM film
WHERE title LIKE '%Truman%';
SELECT COUNT(*) FROM film
WHERE title LIKE '%Truman%';