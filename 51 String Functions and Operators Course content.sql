SELECT * FROM customer;

SELECT LENGTH(first_name) FROM customer;

SELECT first_name || last_name FROM customer;

SELECT first_name || ' ' || last_name AS full_name
FROM customer;

SELECT UPPER(first_name) || ' ' || UPPER(last_name) AS full_name
FROM customer;

SELECT * FROM customer;

SELECT LOWER(first_name) || LOWER(last_name) || '@gmail.com'
FROM customer;

SELECT LEFT(LOWER(first_name),1) || LOWER(last_name) || '@gmail.com'
FROM customer;

SELECT LEFT(LOWER(first_name),1) || '.' || LOWER(last_name) || '@gmail.com'
FROM customer;

SELECT LEFT(LOWER(first_name),1) || '.' || LOWER(last_name) || '@gmail.com'
AS custom_email
FROM customer;