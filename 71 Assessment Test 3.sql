Create a new database called "School" this database should have two tables: teachers and students.

CREATE DATABASE "School"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

CREATE TABLE teachers(
	teacher_id SERIAL PRIMARY KEY,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	homeroom_number INTEGER NOT NULL CHECK (homeroom_number > 0),
	department VARCHAR(50) NOT NULL,
	email VARCHAR(250) UNIQUE NOT NULL,
	phone TEXT UNIQUE NOT NULL CHECK (LENGTH(phone) >= 7),
	created_on TIMESTAMP NOT NULL,
	last_login TIMESTAMP
)

CREATE TABLE students(
	student_id SERIAL PRIMARY KEY,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	homeroom_number INTEGER NOT NULL CHECK (homeroom_number > 0),
	phone TEXT UNIQUE NOT NULL CHECK (LENGTH(phone) >= 7),
	email VARCHAR(250) UNIQUE NOT NULL,
	graduation_year DATE NOT NULL,
	created_on TIMESTAMP NOT NULL,
	last_login TIMESTAMP
)


The students table should have columns for student_id, first_name,last_name, homeroom_number, phone,email, and graduation year.

The teachers table should have columns for teacher_id, first_name, last_name, homeroom_number, department, email, and phone.

The constraints are mostly up to you, but your table constraints do have to consider the following:

1. We must have a phone number to contact students in case of an emergency.
2. We must have ids as the primary key of the tables
3. Phone numbers and emails must be unique to the individual.

Once you've made the tables, insert a student named Mark Mark (student_id=1) who has a phone number of 777-555-1234 and doesn't have an email.
He graduates in 2035 and has 5 as a homeroom number.

INSERT INTO students(
	first_name,
	last_name,
	homeroom_number,
	phone,
	graduation_year,
	created_on
)
VALUES(
	'Mark',
	'Watney',
	'5',
	'777-555-1234',
	2035,
	CURRENT_TIMESTAMP
);

SELECT * FROM students;

Then insert a teacher names Jonas Salk (teacher_id = 1) who as a homeroom number of 5 and is from the Biology department.
His contact info is: jsalk@school.org and a phone number of 777-555-4321.

INSERT INTO teachers(
	first_name,
	last_name,
	homeroom_number,
	department,
	email,
	phone,
	created_on
)
VALUES(
	'Jonas',
	'Salk',
	5,
	'Biology',
	'jsalk@school.org',
	'777-555-4321',
	CURRENT_TIMESTAMP
)

SELECT * FROM teachers;