SELECT * FROM payment;

SELECT EXTRACT(YEAR FROM payment_date)
FROM payment;

SELECT EXTRACT(YEAR FROM payment_date) AS myyear
FROM payment;

SELECT EXTRACT(YEAR FROM payment_date) AS year
FROM payment;

SELECT EXTRACT(MONTH FROM payment_date) AS pay_month
FROM payment;

SELECT EXTRACT(QUARTER FROM payment_date) AS pay_quarter
FROM payment;

SELECT AGE(payment_date)
FROM payment;

SELECT TO_CHAR(payment_date,'YYYY-MM-DD')
FROM payment;

SELECT TO_CHAR(payment_date,'MONTH-YYYY')
FROM payment;

SELECT TO_CHAR(payment_date,'mm/dd/YYYY')
FROM payment;