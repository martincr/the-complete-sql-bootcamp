# General syntax.

ALTER TABLE table_name
DROP COLUMN table_name;

# Remove all dependencies.

ALTER TABLE table_name
DROP COLUMN table_name CASCADE;

# Check for existence to avoid error.

ALTER TABLE table_name
DROP COLUMN IF EXISTS col_name;

ALTER TABLE new_info
DROP COLUMN people;

SELECT * FROM new_info;