SELECT staff_id,COUNT(payment_id) FROM payment
GROUP BY staff_id
ORDER BY COUNT(payment_id);
SELECT staff_id,COUNT(*) FROM payment
GROUP BY staff_id;
SELECT * FROM film;
SELECT rating,AVG(replacement_cost) FROM film
GROUP BY rating;
SELECT rating,ROUND(AVG(replacement_cost),2) FROM film
GROUP BY rating;
SELECT * FROM payment;
SELECT customer_id,SUM(amount) FROM payment
GROUP BY customer_id
ORDER BY SUM(amount) DESC
LIMIT 5;