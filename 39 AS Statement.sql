# Note: AS alias only assigned at the very end: cannot use to filter by (WHERE, HAVING).

SELECT customer_id FROM payment;

SELECT amount AS rental_price
FROM payment;

SELECT SUM(amount) AS net_revenue
FROM payment;

SELECT COUNT(*) AS num_transactions
FROM payment;

SELECT customer_id,SUM(amount) AS total_spent
FROM payment
GROUP BY customer_id;

SELECT customer_id,SUM(amount) AS total_spent
FROM payment
GROUP BY customer_id
HAVING SUM(amount) > 100;