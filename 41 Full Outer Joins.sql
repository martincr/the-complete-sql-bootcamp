# Note: Values present in only one of the tables being joined.

Examples:

- FULL OUTER JOIN
- FULL OUTER JOIN with WHERE

SELECT * FROM payment;

SELECT * FROM customer;

SELECT * FROM customer
FULL OUTER JOIN payment
ON customer.customer_id = payment.customer_id;

SELECT * FROM customer
FULL OUTER JOIN payment
ON customer.customer_id = payment.customer_id
WHERE customer.customer_id IS null
OR payment.payment_id IS null;

SELECT COUNT(DISTINCT customer_id) FROM payment;

SELECT COUNT(DISTINCT customer_id) FROM customer;