SELECT * from payment
LIMIT 2;
SELECT DISTINCT (amount) from payment;
SELECT DISTINCT (amount) from payment
ORDER BY amount;
SELECT * from payment
WHERE amount IN (0.99,1.98,1.99);
SELECT COUNT(*) from payment
WHERE amount IN (0.99,1.98,1.99);
SELECT COUNT(*) from payment
WHERE amount NOT IN (0.99,1.98,1.99);
SELECT * from customer;
SELECT * from customer
WHERE first_name IN ('John','Jake','Julie');
SELECT * from customer
WHERE first_name NOT IN ('John','Jake','Julie');