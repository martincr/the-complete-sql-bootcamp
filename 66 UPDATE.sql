UPDATE account
SET last_login = CURRENT_TIMESTAMP
WHERE last_login IS NULL;

UPDATE account
SET last_login = CURRENT_TIMESTAMP;

UPDATE account
SET last_login = created_on;

UPDATE account
SET last_login = created_on
RETURNING account_id,last_login;

SELECT * FROM account;

UPDATE account
SET last_login = CURRENT_TIMESTAMP;

UPDATE account
SET last_login = created_on;

SELECT * FROM job;

SELECT * FROM account_job;

# Update join example.

UPDATE account_job
SET hire_date = account.created_on
FROM account
WHERE account_job.user_id = account.user_id;

SELECT * FROM account_job;

SELECT * FROM account;

UPDATE account
SET last_login = CURRENT_TIMESTAMP
RETURNING email,last_login,created_on;