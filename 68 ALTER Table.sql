# Syntax examples.

ALTER TABLE table_name action

ALTER TABLE table_name
ADD COLUMN new_col TYPE

ALTER TABLE table_name
DROP COLUMN col_name

ALTER TABLE table_name
ALTER COLUMN col_name
SET default_value

CREATE TABLE information(
	info_id SERIAL PRIMARY KEY,
	title VARCHAR(500) NOT NULL,
	person VARCHAR(50) NOT NULL UNIQUE
);

SELECT * FROM information;

ALTER TABLE information
RENAME TO new_info;

SELECT * FROM new_info;

ALTER TABLE new_info
RENAME COLUMN person TO people;

# Deliberate error.

INSERT INTO new_info(
title)
VALUES
('Some new title');

ALTER TABLE new_info
ALTER COLUMN people DROP NOT NULL;