
- What are the emails of the customers who live in California?

SELECT * FROM customer;

SELECT customer.email FROM customer;

SELECT * FROM address;

SELECT address.district FROM address;

SELECT customer.email,address.district FROM customer,address;

SELECT address.district,email
FROM address
LEFT OUTER JOIN customer ON
customer.address_id = address.address_id;

SELECT address.district,customer.email
FROM address
LEFT OUTER JOIN customer ON
customer.address_id = address.address_id
WHERE district = 'California';

- Get a list of all the movies "Nick Wahlberg" has been in.

SELECT * FROM actor;

SELECT * FROM film;

SELECT film.title FROM film;

SELECT * FROM film_actor;

SELECT film.title,actor.first_name,actor.last_name FROM film,actor;

SELECT film.title,actor.first_name,actor.last_name
FROM film,film_actor
LEFT OUTER JOIN actor ON
actor.actor_id = film_actor.actor_id;

SELECT film.title,actor.first_name,actor.last_name
FROM film,film_actor
LEFT OUTER JOIN actor ON
actor.actor_id = film_actor.actor_id
WHERE actor.actor_id = 2;

# Reversed lookup:

SELECT film.title,actor.first_name,actor.last_name
FROM actor,film_actor
LEFT OUTER JOIN film ON
film.film_id = film_actor.film_id
WHERE actor.actor_id = 2;

# Actual solution:

SELECT * FROM film;

SELECT * FROM actor;

SELECT * FROM film_actor;

SELECT * FROM actor
LEFT OUTER JOIN film_actor
ON actor.actor_id = film_actor.actor_id;

# Two joins:

SELECT * FROM actor
INNER JOIN film_actor
ON actor.actor_id = film_actor.actor_id
INNER JOIN film
ON film_actor.film_id = film.film_id;

SELECT title,first_name,last_name FROM actor
INNER JOIN film_actor
ON actor.actor_id = film_actor.actor_id
INNER JOIN film
ON film_actor.film_id = film.film_id;

# Two joins, and a filter:

SELECT title,first_name,last_name FROM actor
INNER JOIN film_actor
ON actor.actor_id = film_actor.actor_id
INNER JOIN film
ON film_actor.film_id = film.film_id
WHERE first_name = 'Nick'
AND last_name = 'Wahlberg';