# Syntax examples.

DELETE FROM

DELETE FROM
USING
WHERE

DELETE FROM table

SELECT * FROM job;

INSERT INTO job(job_name)
VALUES "Cowboy";

DELETE FROM job
WHERE job_name = 'Cowboy'
RETURNING job_id,job_name;
