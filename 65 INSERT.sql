SELECT * FROM account;

INSERT INTO account(username,password,email,created_on)
VALUES ('Jose','password','jose@mail.com',CURRENT_TIMESTAMP);

SELECT * FROM job;

INSERT INTO job(job_name)
VALUES('Astronaut');

INSERT INTO job(job_name)
VALUES('President');

SELECT * FROM account_job;

INSERT INTO account_job(user_id,job_id,hire_date)
VALUES(1,1,CURRENT_TIMESTAMP);

# Deliberate error to test for foreign key constraints.

INSERT INTO account_job(user_id,job_id,hire_date)
VALUES(10,10,CURRENT_TIMESTAMP);